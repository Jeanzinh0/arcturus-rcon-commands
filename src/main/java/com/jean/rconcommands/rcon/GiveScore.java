package com.jean.rconcommands.rcon;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.messages.outgoing.rooms.users.RoomUserDataComposer;
import com.eu.habbo.messages.rcon.RCONMessage;
import com.google.gson.Gson;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Jean on 05/02/2017.
 */
public class GiveScore extends RCONMessage<GiveScore.JSONGiveScore>
{
    public GiveScore()
    {
        super(GiveScore.JSONGiveScore.class);
    }

    public void handle(Gson gson, GiveScore.JSONGiveScore object) {
        Habbo habbo = Emulator.getGameEnvironment().getHabboManager().getHabbo(object.user_id);
        //online user
        if(habbo != null) {
            habbo.getHabboStats().addAchievementScore(object.score_amount);
            if(habbo.getHabboInfo().getCurrentRoom() != null) {
                habbo.getHabboInfo().getCurrentRoom().sendComposer((new RoomUserDataComposer(habbo)).compose());
            }
        }
        //offline user
        else
        {
            PreparedStatement statement = Emulator.getDatabase().prepare("UPDATE users_settings SET achievement_score = achievement_score + ? WHERE id = ? LIMIT 1");
            try {
                statement.setInt(1, object.score_amount);
                statement.setInt(2, object.user_id);
                statement.execute();
                statement.close();
                statement.getConnection().close();
            } catch (SQLException var6) {
                this.status = 4;
                Emulator.getLogging().logSQLException(var6);
            }

            this.message = "offline";
        }
    }
    public class JSONGiveScore {
        public int user_id;
        public int score_amount;

        public JSONGiveScore() {
        }
    }
}
