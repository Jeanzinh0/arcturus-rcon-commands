package com.jean.rconcommands;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;
import com.jean.rconcommands.rcon.GiveScore;

/**
 * Created by Jean on 05/02/2017.
 */
public class RCONCommands extends HabboPlugin implements EventListener
{
    @Override
    public void onEnable()
    {
        Emulator.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable()
    {

    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event) throws Exception {
        Emulator.getRconServer().addRCONMessage("givescore", GiveScore.class);
    }

}
